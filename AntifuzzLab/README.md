# Explanation

## Vanilla

The vanilla directory contains the basic code test.c of our tests: a simple program searching the string "test" in a file given as argument.

## Antifuzzed

The test program and source code after being antifuzzed by Antifuzz using the antifuzz.h file generated with the command line:

```bash
$ ./AntiFuzz/antifuzz.py --anti-coverage 100 --signal  --crash-action exit
```

## Antifuzz + Tigress

The antifuzzed source code and binary after the application of Tigress using specific options describe in each directory.

## Antifuzz + Tigress -> Reverse

The binary from the corresponding Antifuzzed + Tigress directory after the emputation of the antifuzz_init function to bypass the antifuzzing techniques. This is done by replacing the call/jump by RET or NOP instructions for now.