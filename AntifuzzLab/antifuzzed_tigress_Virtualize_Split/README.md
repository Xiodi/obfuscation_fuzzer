```bash
$ tigress --Environment=x86_64:Linux:Gcc:4.6 --Seed=0 --Transform=Virtualize --Functions=antifuzz_init,main \
    --Transform=Split --SplitKinds=deep,block,top --SplitCount=100 --Functions=antifuzz_init,main \
    --out=antifuzzed_tigress.c ../vanilla/test.c
```
