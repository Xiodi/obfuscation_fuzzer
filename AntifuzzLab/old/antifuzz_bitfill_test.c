#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "antifuzz.h"

int main(int argc,char **argv){
  char pass[20];
  char *p = "test";
  int fd;
  int n;

  antifuzz_init(argv[1],FLAG_ALL);
  fd = open(argv[1],O_RDONLY);
  read(fd,pass,40);

  if(pass[0] == 't' && pass[1] == 'e' && pass[2] == 's' && pass[3] == 't')
    printf("win\n");
  else 
    printf("loose\n");
  close(fd);
	
  return 0;
}
