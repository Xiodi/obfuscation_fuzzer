```bash
$ tigress --Environment=x86_64:Linux:Gcc:4.6 --Seed=0 --Transform=Virtualize --Functions=main \
    --Transform=Split --SplitKinds=deep,block,top --SplitCount=100 --Functions=main \
    --out=test_tigress.c ../vanilla/test.c
```
