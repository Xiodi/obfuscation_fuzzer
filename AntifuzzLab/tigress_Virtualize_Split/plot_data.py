import sys
import matplotlib.pyplot as plt

with open('afl_out/plot_data', 'r') as f:
    lines = f.readlines()
    x = [float(line.split(",")[0])-float(lines[1].split(",")[0]) for line in lines[1:]]
    y = [float(line.split(",")[3]) for line in lines[1:]]
plt.xlabel('time (s)')
plt.ylabel('number of paths found')
plt.title('Tigress alone (Virtualize+Split)')
plt.plot(x ,y)
plt.show()
